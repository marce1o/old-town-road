const DISPLAY_SHOW = 'block';
const DISPLAY_FLEX = 'flex';
const TIER_GOLD = 'gold';
const TIER_SILVER = 'silver';
const NO_TIER = 'burnt';
const IMAGES_FOLDER = 'images/';
const IMAGE_EXTENSION = '.png';
const STORAGE_KEY = 'my_otr';
const ARM_ANIMATION = 'upanddown';
const MODE_TUTORIAL = 'tutorial';
const MODE_PLAY = 'play';
const MODE_END = 'end';
const MODE_GAME_OVER = 'gameover';
const MODE_INTRO = 'intro';
const MODE_RESTART = 'restart';


var character = document.getElementById('character');
var arm = document.getElementById('arm');
var rp = document.getElementById('rotation');
var object = document.getElementById('object');
var scoreText = document.getElementById('score');
var livesPanel = document.getElementById('lives');
var galleryPanel = document.getElementById('gallery');
var dialogPanel = document.getElementById('dialog');

var bullets = [];
var shots = 0;
var hits = 0;
var waves = 0;
var score = 0;
var lives = config.initial_lives;
var objectSpawnID = -1;
var objectAnimationID = -1;
var unseenObjects = [];
var seenObjects = [];
var state = {};
var gallery = {};
var gameMode = MODE_INTRO;
var tutorialStep = 0;

document.body.addEventListener('click', function(event) {
  clickOrTouch(event.clientX, event.clientY);
}, false);

loadState();
loadSounds();
loadLives();
loadGallery();
loadBullets();

playIntro();

function playIntro() {
  showDialog(config.messages.intro);
}

function startGame() {

  gameMode = MODE_PLAY;

  updateLives();
  updateScore();
  updateGallery();

  hideDialog();

  unseenObjects = config.valid_objects.slice();
  seenObjects = config.invalid_objects.slice();

  shots = 0;
  hits = 0;
  waves = 0;

  theme.play();

  configObjectSpawner();

}

function clickOrTouch(x, y) {

  if (gameMode == MODE_INTRO) {

    show(galleryPanel);
    show(scoreText);
    show(character);
    gallop.play();

    if (state.tutorial < config.minimum_tutorial_plays) {
      stepTutorial(1);
    } else {
      startGame();
    }

  } else if (gameMode == MODE_PLAY || (gameMode == MODE_TUTORIAL && tutorialStep != 8)) {

    if (x > window.innerWidth * config.left_screen_offset_ignore_user_input) {
      var bullet = pickBullet();
      if (bullet != null) {
        animateArm(x, y);
        setTimeout(function() {
          shotSFX.play();
          shoot(bullet, x, y);
        }, config.arm_animation_up_offset + config.arm_animation_aim_offset / config.arm_animation_duration);
      }
    }

  } else if (gameMode == MODE_TUTORIAL && tutorialStep == 8) {
    startGame();
  } else if (gameMode == MODE_RESTART) {
    startGame();
  }

}

function stepTutorial(step) {

  tutorialStep = step;

  if (step == 1) {

    gameMode = MODE_TUTORIAL;

    state.tutorial++;
    saveState();

    updateLives();
    updateScore();
    updateGallery();

    showDialog(config.messages.tutorial.step1);

  } else if (step == 2) {

    objectSpawnID = window.setInterval(function() {
      spawnForTutorial(config.valid_objects);
      fly();
    }, config.interval_between_object_spawn)

    showDialog(config.messages.tutorial.step2);

  } else if (step == 3) {

    showDialog(config.messages.tutorial.step3);

  } else if (step == 4) {

    showDialog(config.messages.tutorial.step4);

  } else if (step == 5) {

    showDialog(config.messages.tutorial.step5);

    clearInterval(objectSpawnID);

    objectSpawnID = window.setInterval(function() {

      spawnForTutorial(config.invalid_objects);

      fly();

    }, config.interval_between_object_spawn);

  } else if (step == 6) {

    showDialog(config.messages.tutorial.step6);

    window.setTimeout(stepTutorial, config.messages.tutorial.step6.duration, 7);

  } else if (step == 7) {

    hide(object);

    clearInterval(objectSpawnID);

    showDialog(config.messages.tutorial.step7);

    window.setTimeout(stepTutorial, config.messages.tutorial.step7.duration, 8);

  }

}

function spawn() {
  waves++;

  if (waves > config.total_waves) {

    endGame();

  } else {

    object.data = {
      name: pickObject(),
      speed: getRandomArbitrary(eval(config.minimun_object_speed), eval(config.maximum_object_speed)),
      y: getRandomArbitrary(eval(config.minimun_object_y), eval(config.maximum_object_y)),
      size: getRandomArbitrary(eval(config.minimun_object_size), eval(config.maximum_object_size)),
      active: true
    };

    configureObject();
    show(object);

  }

}

function spawnForTutorial(objects) {
  waves++;
  object.data = {
    name: objects[getRandomInt(0, objects.length / 2)],
    speed: config.tutorial_object_speed,
    y: getRandomInt(eval(config.tutorial_object_min_y), eval(config.tutorial_object_min_y)),
    size: config.tutorial_object_size,
    active: true
  };
  configureObject();
  show(object);
}

function pickObject() {

  var name = undefined;
  // If all objects were seen, pick from the seen list
  if (unseenObjects.length == 0) {
    return seenObjects[getRandomInt(0, seenObjects.length)];
  }
  // The unseen objects list must be empty by the last wave
  if (unseenObjects.length >= (config.total_waves - waves)) {
    return unseenObjects.shift();
  }
  // Randomize
  var newOrNot = Math.random();

  if (newOrNot <= config.new_object_chance) {
    name = unseenObjects.shift();
    seenObjects.push(name);
  } else {
    name = seenObjects[getRandomInt(0, seenObjects.length)];
  }

  return name;
}

function fly() {
  var lastTime = new Date().getTime();

  clearInterval(objectAnimationID);

  objectAnimationID = window.setInterval(frame, config.object_animation_interval);

  function frame() {
    var elapsed = new Date().getTime() - lastTime;

    lastTime = new Date().getTime();

    var left = getX(object);

    if (left < config.screen_y_limit_for_object_animation) {
      object.data.active = false;
      clearInterval(objectAnimationID);
      hide(object);
    } else {
      left = left + elapsed * object.data.speed;
      translateTo(object, left, object.data.y);
    }
  }
}

// Parameters: point B
function calculateAngle(bx, by) {
  // Point A
  var ax = rp.getBoundingClientRect().left;
  var ay = rp.getBoundingClientRect().top;

  var dy = by - ay;
  var dx = bx - ax;

  var rads = Math.atan2(dy, dx); // range (-PI, PI]
  var degs = rads * 180 / Math.PI; // rads to degs, range (-180, 180]
  var adjusted = degs - 10.0;

  return {
    rads,
    degs,
    adjusted
  };
}

function shoot(bullet, x, y) {

  shots++;

  var angle = calculateAngle(x, y);

  var left = rp.getBoundingClientRect().left;
  var top = rp.getBoundingClientRect().top

  left += Math.cos(angle.rads) * config.base_arm_length;
  top += Math.sin(angle.rads) * config.base_arm_length;

  var speed = config.bullet_speed;

  var lastTime = new Date().getTime();

  var ratio = (x - left) / (top - y);

  var speedX = speed;
  var speedY = speedX / ratio;

  translateTo(bullet, left, top);
  show(bullet);

  bullet.animationID = window.setInterval(frame, config.bullet_animation_interval);

  function frame() {
    var elapsed = new Date().getTime() - lastTime;
    lastTime = new Date().getTime();

    if (left > window.innerWidth || top < 0 || top > window.innerHeight) {
      clearInterval(bullet.animationID);
      hide(bullet);
      bullet.active = false;
    } else {
      left = left + elapsed * speedX;
      top = top - elapsed * speedY;
      translateTo(bullet, left, top);
      checkCollision(bullet);
    }
  }

  if (gameMode == MODE_TUTORIAL) {
    if (tutorialStep == 1 && shots >= config.minimum_shoots_tutorial_step_1) {
      stepTutorial(2);
    }
  }

}

function checkCollision(bullet) {
  var x1 = getX(bullet);
  var y1 = getY(bullet);
  var x2 = getX(object);
  var y2 = getY(object);
  var w2 = h2 = getW(object);
  if (object.style.display == DISPLAY_SHOW && object.data.active) {
    if (x1 > x2) {
      if (x1 < (x2 + w2)) {
        if (y1 > y2) {
          if (y1 < (y2 + h2)) {
            hit(bullet);
          }
        }
      }
    }
  }
}

function hit(bullet) {

  hits++;

  object.data.active = false;
  clearInterval(objectAnimationID);
  clearInterval(bullet.animationID);
  hide(bullet);

  bullet.active = false;

  if (config.valid_objects.includes(object.data.name)) {
    hitSFX.play();
    updateScore(eval(config.score_on_object_hit));
    updateGallery(object.data.name);
  } else {
    missSFX.play();
    updateLives(config.lifes_updated_on_invalid_hit);
  }

  if (gameMode == MODE_PLAY) {
    saveState();
  } else if (gameMode == MODE_TUTORIAL) {
    if (tutorialStep == 2) {
      stepTutorial(3);
    } else if (tutorialStep == 3 && hits >= config.minimum_hits_tutorial_step_3) {
      stepTutorial(4);
    } else if (tutorialStep == 4) {
      stepTutorial(5);
    } else if (tutorialStep == 5) {
      stepTutorial(6);
    } else if (tutorialStep == 6) {
      if (lives == 1) {
        clearInterval(objectSpawnID);
      }
    }
  }

  if (lives == 0) {
    gameOver();
  }

}

function updateScore(amount) {
  if (amount) {
    score += amount;
  } else {
    score = 0;
  }
  scoreText.innerHTML = currencyFormat(score);
  if (gameMode == MODE_PLAY) {
    if (score > state.highscore) {
      state.highscore = score;
    }
  }
}

function updateGallery(objectToAdd) {

  if (objectToAdd) {
    // Update
    gallery[objectToAdd].element.src = objImgSrc(objectToAdd, TIER_GOLD);

    if (!gallery.golden.includes(objectToAdd)) {
      gallery.golden.push(objectToAdd);
    }

    if (gameMode == MODE_PLAY) {
      if (!state.objects.includes(objectToAdd)) {
        state.objects.push(objectToAdd);
      }
    }
  } else {
    // Initialize
    gallery.golden = [];

    var siverObjects = [];

    if (gameMode == MODE_PLAY) {
      siverObjects = state.objects;
    }

    for (objectName of config.valid_objects) {
      if (siverObjects.includes(objectName)) {
        gallery[objectName].element.src = objImgSrc(objectName, TIER_SILVER);
      } else {
        gallery[objectName].element.src = objImgSrc(objectName, NO_TIER);
      }
    }
  }
}

function saveState() {
  window.localStorage.setItem(STORAGE_KEY, JSON.stringify(state));
}

function objImgSrc(objectName, tier) {
  return IMAGES_FOLDER + objectName + '-' + tier + IMAGE_EXTENSION;
}

function currencyFormat(num) {
  return '$ ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function updateLives(amount) {
  if (amount) {
    lives = lives + amount;
  } else {
    lives = config.initial_lives;
  }
  for (var i = 0; i < livesPanel.children.length; i++) {
    if (i < lives) {
      livesPanel.children[i].style.opacity = 1;
    } else {
      livesPanel.children[i].style.opacity = 0;
    }
  }

}

function gameOver() {
  gameMode = MODE_GAME_OVER;
  window.setTimeout(hide, config.object_hit_animation_duration, object);
  clearInterval(objectSpawnID);
  showDialog(config.messages.gameOver);
  // animate character back

  stopMusic(theme, config.messages.gameOver.duration);

  window.setTimeout(function() {
    gameMode = MODE_RESTART;
    showDialog(config.messages.intro);
  }, config.messages.gameOver.duration);
}

function endGame() {
  gameMode = MODE_END;
  clearInterval(objectSpawnID);
  if (gallery.golden.length == config.valid_objects.length) {
    showDialog(config.messages.fullEndGame);
  } else {
    showDialog(config.messages.endGame);
  }
  // animate character ahead
  // handle gallery full: town animation, credits, try again message
  window.setTimeout(function() {
    gameMode = MODE_RESTART;
    showDialog(config.messages.intro);
  }, config.messages.endGame.duration);
}

function pickBullet() {
  for (var bullet of bullets) {
    if (!bullet.active) {
      bullet.active = true;
      return bullet;
    }
  }
  return null;
}

function show(element) {
  if (element == galleryPanel) {
    element.style.display = DISPLAY_FLEX;
  } else {
    element.style.display = DISPLAY_SHOW;
  }
}

function hide(object) {
  object.style.display = "none";
}

function translateTo(object, x, y) {
  object.style.top = y + "px";
  object.style.left = x + "px";
}

function scaleTo(object, size) {
  object.style.width = size + 'px';
  object.style.height = size + 'px';
}

function getX(object) {
  return Number.parseFloat(object.style.left);
}

function getY(object) {
  return Number.parseFloat(object.style.top);
}

function getW(object) {
  return Number.parseFloat(object.style.width);
}

function configureObject() {
  translateTo(object, window.innerWidth, object.data.y);
  scaleTo(object, object.data.size);
  object.style['background-image'] = 'url(images/' + object.data.name + '.png)';
}

function configObjectSpawner() {

  if (gameMode == MODE_PLAY) {


    window.setTimeout(function() {
      objectSpawnID = window.setInterval(function() {
        spawn();
        fly();
      }, config.interval_between_object_spawn)
    }, config.delay_before_first_object_spawn);

  } else {
    clearInterval(objectSpawnID);
  }

}

function loadSounds() {

  function Channel(audio_uri) {
    this.audio_uri = audio_uri;
    this.resource = new Audio(audio_uri);
  }

  function Switcher(audio_uri, num) {
    this.channels = [];
    this.num = num;
    this.index = 0;
    for (var i = 0; i < num; i++) {
      this.channels.push(new Channel(audio_uri));
    }
  }

  Channel.prototype.play = function() {
    // Try refreshing the resource altogether
    this.resource.play();
  }

  Switcher.prototype.play = function() {
    this.channels[this.index++].play();
    this.index = this.index < this.num ? this.index : 0;
  }

  shotSFX = new Switcher('sounds/shot.mp3', config.maximum_parallel_bullets + 3);
  hitSFX = new Switcher('sounds/hit.mp3', 1);
  missSFX = new Switcher('sounds/horse-neigh.mp3', 1);
  theme = new Audio('sounds/theme.mp3');
  theme.volume = 1;
  gallop = new Audio('sounds/gallop.mp3');
  gallop.loop = true;

}

function loadGallery() {
  gallery.golden = [];
  for (objectName of config.valid_objects) {
    gallery[objectName] = {
      element: document.getElementById(objectName)
    };
  }
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

function showDialog(message) {
  hideDialog();
  setTimeout(function() {
    dialogPanel.style.top = message.y;
    dialogPanel.style.left = message.x;
    dialogPanel.innerHTML = message.text;
    dialogPanel.classList.add("dialogIn");
  }, 1000);
}

function hideDialog() {
  dialogPanel.classList.remove("dialogIn");
}

function loadBullets() {
  for (var i = 0; i < config.maximum_parallel_bullets; i++) {
    var bulletElement = document.createElement('div');
    bulletElement.setAttribute('class', 'bullet');
    bulletElement.active = false;
    document.body.appendChild(bulletElement)
    bullets.push(bulletElement);
  }
}

function loadLives() {
  for (var i = 0; i < config.initial_lives; i++) {
    var lifeElement = document.createElement('div');
    lifeElement.setAttribute('class', 'life');
    livesPanel.appendChild(lifeElement)
  }
}

function loadState() {
  state = JSON.parse(window.localStorage.getItem(STORAGE_KEY));
  if (state == null) {
    state = {
      objects: [],
      highscore: 0,
      tutorial: 0
    };
  }
}

function stopMusic(audio, time) {

  volumeDown = window.setInterval(frame, time * config.volume_smooth_decrease);

  function frame() {
    if (audio.volume == 0) {
      audio.pause();
      audio.currentTime = 0;
      audio.volume = 1;
      clearInterval(volumeDown);
    } else {
      audio.volume = Math.max(0, audio.volume - config.volume_smooth_decrease);
    }

  }

}
// ************************************************
// From http://jsfiddle.net/russelluresti/RHhBz/2/

// search the CSSOM for a specific -webkit-keyframe rule
function findKeyframesRule(rule) {
  // gather all stylesheets into an array
  var ss = document.styleSheets;

  // loop through the stylesheets
  for (var i = 0; i < ss.length; ++i) {

    // loop through all the rules
    for (var j = 0; j < ss[i].cssRules.length; ++j) {

      // find the -webkit-keyframe rule whose name matches our passed over parameter and return that rule
      if (ss[i].cssRules[j].type == window.CSSRule.KEYFRAMES_RULE && ss[i].cssRules[j].name == rule)
        return ss[i].cssRules[j];
    }
  }

  // rule not found
  return null;
}

// begin the new animation process
function animateArm(x, y) {
  // remove the old animation from our object
  arm.style.webkitAnimationName = "none";

  // update the keyframe animation: remove old keyframes and add new ones
  setTimeout(function() {
    var angle = calculateAngle(x, y);

    // find our -webkit-keyframe rule
    var keyframes = findKeyframesRule(ARM_ANIMATION);

    // remove the existing 0% and 100% rules
    keyframes.deleteRule(config.arm_animation_up_offset + "%");
    keyframes.deleteRule(config.arm_animation_aim_offset + "%");

    // create new 0% and 100% rules with random numbers
    keyframes.appendRule(config.arm_animation_up_offset + "% { transform: scale(0.26) translate(54px,-115px) rotate(" + angle.adjusted + "deg); }");
    keyframes.appendRule(config.arm_animation_aim_offset + "% { transform: scale(0.26) translate(54px,-115px) rotate(" + angle.degs + "deg); }");

    // assign the animation to our element (which will cause the animation to run)
    arm.style.webkitAnimationName = ARM_ANIMATION;

  }, 0);
}

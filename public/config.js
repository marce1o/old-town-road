var config = {
  delay_before_first_object_spawn: 1000,
  interval_between_object_spawn: 352000 / (100 - 1), // Main song duration / ( number of waves + 2 )
  volume_smooth_decrease: 0.025,
  maximum_parallel_bullets: 1, // 1
  score_on_object_hit: 'waves * 10',
  lifes_updated_on_invalid_hit: -1,
  base_arm_length: 70,
  initial_lives: 3, // 3
  bullet_speed: 600 / 250.0, // pixels per milliseconds
  screen_y_limit_for_object_animation: -100,
  bullet_animation_interval: 1,
  object_animation_interval: 1,
  object_hit_animation_duration: 3000,
  left_screen_offset_ignore_user_input: 0.25,
  arm_animation_duration: 750,
  arm_animation_up_offset: 15,
  arm_animation_aim_offset: 25,
  total_waves: 100, // 100
  new_object_chance: 0.15, // 1 in 7 objects picked should be new to the user
  minimun_object_speed: '-0.4 - waves * (0.9 - 0.4) / config.total_waves',
  maximum_object_speed: '-0.9 - waves * (1.7 - 0.9) / config.total_waves',
  minimun_object_size: '55 - waves * (55 - 25) / config.total_waves',
  maximum_object_size: '65 - waves * (65 - 35) / config.total_waves',
  minimun_object_y: '5 + waves * (5 - 5) / config.total_waves',
  maximum_object_y: '(window.innerHeight / 2.666) + waves * (window.innerHeight / 1.333 - window.innerHeight / 2.666) / config.total_waves',
  valid_objects: ['horse', 'hat', 'boots', 'porsche', 'tractor', 'baby', 'movie', 'bull', 'boobies', 'cowboy', 'gucci', 'wrangler'],
  invalid_objects: ['star', 'cactus', 'gun', 'lips', 'wheel', 'barrel', 'vinyl', 'guitar', 'coffin', 'moneybag'],
  minimum_tutorial_plays: 1, // 1
  minimum_shoots_tutorial_step_1: 3, // 3
  minimum_hits_tutorial_step_3: 3, // 3
  tutorial_object_size: 90, // 90
  tutorial_object_speed: -0.5, // -0.5
  tutorial_object_min_y: '30', // '30'
  tutorial_object_max_y: 'window.innerHeight / 3', // 'window.innerHeight / 3'
  messages: {
    intro: {
      text: 'Click to start',
      x: '50%',
      y: '50%'
    },
    tutorial: {
      step1: {
        text: 'You can click anywhere in front of the cowboy to shoot his gun.',
        x: '75%',
        y: '25%'
      },
      step2: {
        text: 'Can you hit the flying objects?',
        x: '75%',
        y: '25%'
      },
      step3: {
        text: 'When you hit an object, you add it to the board. Keep hitting.',
        x: '75%',
        y: '80%'
      },
      step4: {
        text: 'You also increase your score. Check it out.',
        x: '50%',
        y: '30%'
      },
      step5: {
        text: 'But not all objects should be hit...',
        x: '50%',
        y: '33%'
      },
      step6: {
        text: 'If you hit an invalid object, you lose one life.',
        x: '25%',
        y: '25%',
        duration: 8000
      },
      step7: {
        text: 'Are you ready to collect all the objects?',
        x: '50%',
        y: '50%',
        duration: 3000
      }
    },
    gameOver: {
      text: 'Game Over',
      x: '50%',
      y: '50%',
      duration: 5000
    },
    endGame: {
      text: 'You made it. Try to collect all objects now.',
      x: '50%',
      y: '50%',
      duration: 7000
    },
    fullEndGame: {
      text: 'Welcome to Old Town. You made it!',
      x: '50%',
      y: '50%',
      duration: 7000
    }
  }
};

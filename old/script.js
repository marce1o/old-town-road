// var arm = document.getElementById('arm');
// var arm = document.getElementById('svg-test');
var arm = document.getElementById('g-test');




var degree = 0;

function onClick(event) {
  var x = event.clientX;
  var y = event.clientY;

  degree = -angle(0, 0, x - 600, 200 - y);

  console.log({x, y, degree});


  // arm.setAttributeNS(null, 'transform-origin', 'center');
  arm.setAttributeNS(null, 'transform', 'scale(0.2)  translate (7,-18) rotate(' + degree + ')');

  // arm.style.left = x;

}

function angle(cx, cy, ex, ey) {
  var dy = ey - cy;
  var dx = ex - cx;
  var theta = Math.atan2(dy, dx); // range (-PI, PI]
  theta *= 180 / Math.PI; // rads to degs, range (-180, 180]
  //if (theta < 0) theta = 360 + theta; // range [0, 360)
  return theta;
}

function shoot2(x, y) {
  // console.log({x, y});

  var bullet = pickBullet();

  if (bullet != null) {
    // Calculate the source Calculate delta X Calculate delta Y Animate Change bullet status
    var left = 680;
    var top = 280;
    var deltaX = 10;
    var deltaY = 0;

    moveBullet(bullet, left, top);
    showBullet(bullet);

    var id = setInterval(frame, 1);

    function frame() {
      if (left > 1280) {
        clearInterval(id);
        hideBullet(bullet);
        releaseBullet(bullet);
      } else {
        left = left + deltaX;
        moveBullet(bullet, left, top);
      }
    }
  }
}

# Old Town Road

Study inspired on the homonymous hit song. Play at papel.games/tribute

## BACKLOG

### ALPHA
- ✅ Increase difficulty with time (object speed, size and type)
- ✅ Add more objects
- ✅ Add gallery to collect different items
    - ✅ Collected once in a lifetime: grey shape / Collected during session: gold
- ✅ Tutorial
- ✅ Make texts and numbers from tutorial part of config
- ✅ Adjust sound play - no autoplay
- ✅ Handle Game Over, Game End and Restart
- ✅ Create padding for score and lives
- ✅ Object scores dynamically calculated from wave

### BETA/POLISH

- ✅ Test on mobile
    - ✅ Overflow when object is added
    - ✅ No response for touch
    - ✅ No sound
    - ✅ Assets pre-loading

### RELEASE 1
- Adjust colors for gallery
- Ad shadow to board
- Legal disclaimer
- Adjust SFX loudness
- Animate background colors to get darker. The darkest colors should match the "darkest" moments of the song
- Animate Game Over and Restart
- Animate End of Game - Welcome to Old Town

### RELEASE 1.1
- Add intro animation
- Add track map
- Add hit rate
- Shining, moving stars in the sky
- Gracefully add hit items to gallery
- Change cursor
- Dynamically build gallery board
- Show high score (score, hit hate, gallery)

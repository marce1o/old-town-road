## split animated gif into gif frames
convert animated_horse.gif -scene 1 +adjoin  frames/frame_%03d.gif
## add transparency to each gif frame
convert frames/frame_*.gif -fill none -alpha set -channel RGBA -fuzz 50000 -opaque white  frames/f%03d.png
## compose sprite with png frames
montage frames/f00?.png -background none -geometry +0+0 -tile 6x1 horse2.png



## sprite creation
montage frames/f00?.png -background none -geometry +0+0 -tile 6x1 horse2.png

## draw rectangle
convert -size 200x200 xc:#C0C0C0 silver.png

## padding an image
convert original.png -set filename:original %t -gravity center -background none -extent 200x200 "%[filename:original]-new.png"

## apply texture to png
composite -compose Dst_In -gravity center shape.png texture.png -alpha Set textured-shape.png
ls *.png | xargs -I % composite -compose Dst_In -gravity center % ~/gold.png -alpha Set gold-%

## Preload assets
cd /Users/marcelo/Documents/studio/projects/oldtownroad/public
find images -name "*.png" | xargs -I XXX echo '<link rel="preload" href="XXX" as="image" type="image/png">'
find fonts -name "*.woff" | xargs -I XXX echo '<link rel="preload" href="XXX" as="font" type="font/woff">'
find fonts -name "*.woff2" | xargs -I XXX echo '<link rel="preload" href="XXX" as="font" type="font/woff2">'
find sounds -name "*.mp3" | xargs -I XXX echo '<link rel="preload" href="XXX" as="audio" type="audio/mpeg">'

## gold - #FFD700
## silver - #C0C0C0
## burnt - #8B4513

# horse
# hat
# boots
# porsche
# tractor
# baby
# movie
# bull
# boobies
# cowboy
# gucci
# wrangler
